define('my-comments-plugin/comments-per-user-widget', [
    'my-comments-plugin/common-widget',
    'my-comments-plugin/helpers',
    'bitbucket/util/navbuilder'
], function (CommonWidget, helpers, navbuilder) {
    function CommentsPerUserWidget() {
        // Comment[]
        var userCommentsGroups = [];
        // number
        var maxCommentsCount;
        var scaleFactor = 1;
        var animation = true;

        // getters/setters fot these could be added as well
        var userHeight = 20;
        var barHeight = 25;
        var rowPadding = 10;
        var cellPadding = 15;

        var commonWidget = CommonWidget().widgetClass('comments-per-user-widget');

        function render(rootSelection) {
            commonWidget(rootSelection);

            rootSelection.classed('hidden', !userCommentsGroups.length);

            var svgSelection = rootSelection.select('svg');
            if (svgSelection.empty()) {
                svgSelection = rootSelection
                    .select('div.common-widget-body')
                    .append('svg')
                    .call(renderSvgLayout);
            }

            var tableSelection = svgSelection
                .select('g.cpuw-table')
                .attr('transform', 'scale(' + scaleFactor + ')');

            var rowDataSelection = tableSelection.selectAll('g.cpuw-row')
                .data(userCommentsGroups, function (group) {
                    return group.user.id;
                });

            rowDataSelection.exit().remove();

            var newRowsSelection = rowDataSelection.enter()
                .append('g')
                .classed('cpuw-row', true);

            // add two cells for each row
            newRowsSelection
                .selectAll('g.cpuw-cell')
                .data(function (d) { return [d, d]; })
                .enter()
                .append('g')
                .classed('cpuw-cell', true)
                .each(function (group, index) {
                    var selection = d3.select(this);
                    switch (index) {
                        case 0: return renderUser(selection, group.user);
                        case 1: return renderBarChart(selection);
                    }
                });

            // Update all rows/cells below:
            var rowsSelection = tableSelection.selectAll('g.cpuw-row');
            var alignData = alignRowsCells(rowsSelection);

            svgSelection.attr('height', alignData.tableHeight * scaleFactor);

            rowsSelection.select('title.cpuw-bar-chart-title')
                .text(function (group) {
                    return group.comments.length + ' comment(s)';
                });

            rowsSelection.select('rect.cpuw-bar-chart-bar')
                .transition()
                .duration(animation ? 500 : 0)
                .attr('width', function (group) {
                    var cellWidth = getTableWidth(this) - alignData.barChartIndent;
                    return (group.comments.length / maxCommentsCount) * cellWidth;
                });
        }

        function renderSvgLayout(svgSelection) {
            svgSelection.append('g')
                .classed('cpuw-table', true);

            var defsSelection = svgSelection.append('defs');

            defsSelection.append('clipPath')
                .attr('id', 'profilePictureCircle')
                .append('circle')
                .attr('cx', userHeight / 2)
                .attr('cy', userHeight / 2)
                .attr('r', userHeight / 2);
        }

        function renderUser(cellSelection, user) {
            var wrapSelection = cellSelection
                .classed('cpuw-user-cell', true)
                .append('g')
                // vertical align middle
                .attr('transform', 'translate(0,' + -userHeight / 2 + ')');

            wrapSelection.append('image')
                .attr('width', userHeight)
                .attr('height', userHeight)
                .attr('clip-path', 'url(#profilePictureCircle)')
                .attr('href', getUserAvatarHref(user));

            wrapSelection.append('text')
                .classed('cpuw-user-cell-name', true)
                .text(user.displayName)
                .attr('x', userHeight * 1.5)
                .attr('y', userHeight / 2);
        }

        function renderBarChart(cellSelection) {
            var wrapSelection = cellSelection
                .classed('cpuw-bar-chart-cell', true)
                .append('g')
                // vertical align middle
                .attr('transform', 'translate(0,' + -barHeight / 2 + ')');

            wrapSelection.append('rect')
                .classed('cpuw-bar-chart-bar', true)
                .attr('height', barHeight);

            wrapSelection.append('title')
                .classed('cpuw-bar-chart-title', true);
        }

        function getMaxUserCellWidth(userCellsSelection) {
            var maxUserWidth = 0;

            userCellsSelection.each(function () {
                var width = this.getBoundingClientRect().width / scaleFactor;
                if (width > maxUserWidth) {
                    maxUserWidth = width;
                }
            });

            return maxUserWidth;
        }

        function alignRowsCells(rowsSelection) {
            var userCellsSelection = rowsSelection.select('g.cpuw-user-cell');

            var barChartIndent = getMaxUserCellWidth(userCellsSelection) + cellPadding;
            var rowHeight = Math.max(userHeight, barHeight);
            var rowOuterHeight = rowHeight + rowPadding;
            var tableHeight = Math.max(rowOuterHeight * rowsSelection.size() - rowPadding, 0);

            rowsSelection.attr('transform', function (d, index) {
                // add 0.5 rowHeight to align row vertically
                return 'translate(0,' + (index * rowOuterHeight + 0.5 * rowHeight)  + ')';
            });

            rowsSelection.select('g.cpuw-bar-chart-cell')
                .attr('transform', 'translate(' + barChartIndent + ',0)');

            return {
                barChartIndent: barChartIndent,
                tableHeight: tableHeight
            };
        }

        function getTableWidth(innerNode) {
            return innerNode.ownerSVGElement.clientWidth / scaleFactor;
        }

        function getUserAvatarHref(user) {
            return navbuilder.user(user.slug)
                .addPathComponents('avatar.png')
                .build();
        }

        function setMaxCommentsCount() {
            maxCommentsCount = userCommentsGroups.reduce(function (max, group) {
                return Math.max(max, group.comments.length);
            }, 0);
        }

        render.title = helpers.inheritGetterSetter(render, commonWidget.title);

        render.animation = function (x) {
            if (!arguments.length) return x;
            animation = x;
            return render;
        };

        render.userCommentsGroups = function (x) {
            if (!arguments.length) return x;
            userCommentsGroups = x;
            setMaxCommentsCount();
            return render;
        };

        render.scaleFactor = function (x) {
            if (!arguments.length) return x;
            scaleFactor = x;
            return render;
        };

        return render;
    }

    return CommentsPerUserWidget;
});
