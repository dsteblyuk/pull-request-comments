define('my-comments-plugin/bootstrap-comments-tab', [
    'bitbucket/internal/model/page-state',
    'bitbucket/util/navbuilder',
    'bitbucket/util/server',
    'my-comments-plugin/helpers',
    'my-comments-plugin/comments-count-widget',
    'my-comments-plugin/comments-per-user-widget'
],
function (pageState, navbuilder, server, helpers, CommentsCountWidget, CommentsPerUserWidget) {
    var commentsCountWidget = CommentsCountWidget()
        .title(AJS.I18n.getText('my.comments.plugin.comments.count.widget.title'));

    var commentsPerUserWidget = CommentsPerUserWidget()
        .title(AJS.I18n.getText('my.comments.plugin.comments.per.user.widget.title'));

    function renderWidget(rootSelector, widget) {
        d3.selectAll(rootSelector).call(widget);
    }

    function renderAllWidgets(animation) {
        var scaleFactor = AJS.$(window).width() > 1900 ? 1.5 : 1;

        renderWidget('#comments_count_widget_root', commentsCountWidget);
        renderWidget('#comments_per_user_widget_root', commentsPerUserWidget
            .animation(animation)
            .scaleFactor(scaleFactor)
        );
    }

    function updateWidgetsData(allActivities) {
        var allComments = helpers.getAllAddedComments(allActivities);
        var userCommentsGroups = helpers.groupCommentsByUser(allComments);

        commentsCountWidget.count(allComments.length);
        commentsPerUserWidget.userCommentsGroups(userCommentsGroups);
    }

    function getActivitiesUrl() {
        return navbuilder.rest()
            .projects()
            .addPathComponents(pageState.getProject().getKey())
            .addPathComponents('repos', pageState.getRepository().getSlug())
            .addPathComponents('pull-requests', pageState.getPullRequest().getId())
            .addPathComponents('activities')
            .withParams({ limit: 100 })
            .build();
    }

    function bootstrapCommentsTab() {
        AJS.$(window).on('resize', function () {
            renderAllWidgets(false);
        });

        AJS.toInit(function () {
            renderAllWidgets(false);

            server.rest({ url: getActivitiesUrl() })
                .then(function (res) {
                    updateWidgetsData(res.values);
                    renderAllWidgets(true);
                });
        });
    }

    return {
        bootstrapCommentsTab: bootstrapCommentsTab
    };
});
