define('my-comments-plugin/common-widget', function () {
    function CommonWidget() {
        var title;
        var widgetClass;

        function render(rootSelection) {
            var widgetSelection = rootSelection.select('.common-widget-header');

            if (widgetSelection.empty()) {
                widgetSelection = rootSelection
                    .append('div')
                    .classed('common-widget', true)
                    .classed(widgetClass, true);

                widgetSelection
                    .append('div')
                    .classed('common-widget-header', true);

                widgetSelection
                    .append('hr')
                    .classed('common-widget-delimiter', true);

                widgetSelection
                    .append('div')
                    .classed('common-widget-body', true);
            }

            widgetSelection
                .select('.common-widget-header')
                .text(title);
        }

        render.title = function (x) {
            if (!arguments.length) return x;
            title = x;
            return render;
        };

        render.widgetClass = function (x) {
            if (!arguments.length) return x;
            widgetClass = x;
            return render;
        };

        return render;
    }

    return CommonWidget;
});
