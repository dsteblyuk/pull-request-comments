define('my-comments-plugin/comments-count-widget', [
    'my-comments-plugin/common-widget',
    'my-comments-plugin/helpers'
], function (CommonWidget, helpers) {
    function CommentsCountWidget() {
        // Number
        var count;

        var commonWidget = CommonWidget().widgetClass('comments-count-widget');

        function render(rootSelection) {
            commonWidget(rootSelection);

            rootSelection.select('.common-widget-body')
                .text(count == null ? '' : count);
        }

        render.title = helpers.inheritGetterSetter(render, commonWidget.title);

        render.count = function (x) {
            if (!arguments.length) return x;
            count = x;
            return render;
        };

        return render;
    }

    return CommentsCountWidget;
});
