define('my-comments-plugin/helpers', function () {
    function getAllAddedComments(activities) {
        return activities.filter(function (value) {
            return value.commentAction === 'ADDED';
        })
        .map(function (activity) {
            return activity.comment;
        })
        .reduce(function addComment(all, comment) {
            all.push(comment);
            comment.comments.forEach(function (commentReply) {
                addComment(all, commentReply);
            });
            return all;
        }, []);
    }

    function groupCommentsByUser(allComments) {
        return allComments.slice(0)
            // sorting for faster grouping
            .sort(function (a, b) {
                return a.author.id - b.author.id;
            })
            .reduce(function (groups, comment) {
                var lastGroup = groups[groups.length - 1];

                if (lastGroup && lastGroup.user.id === comment.author.id) {
                    lastGroup.comments.push(comment);
                } else {
                    groups.push({
                        user: comment.author,
                        comments: [comment]
                    });
                }
                return groups;
            }, []);
    }

    function inheritGetterSetter(self, superGetterSetter) {
        return function (x) {
            if (!arguments.length) return superGetterSetter();
            superGetterSetter(x);
            return self;
        };
    }

    return {
        getAllAddedComments: getAllAddedComments,
        groupCommentsByUser: groupCommentsByUser,
        inheritGetterSetter: inheritGetterSetter
    };
});
