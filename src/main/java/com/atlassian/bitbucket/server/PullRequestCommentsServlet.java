package com.atlassian.bitbucket.server;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.google.common.collect.ImmutableMap;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.plugin.webresource.WebResourceManager;

import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.atlassian.bitbucket.server.Constants.*;

public class PullRequestCommentsServlet extends HttpServlet {
    private final RepositoryService repositoryService;
    private final PullRequestService pullRequestService;
    private final SoyTemplateRenderer soyTemplateRenderer;

    public PullRequestCommentsServlet(
        SoyTemplateRenderer soyTemplateRenderer,
        RepositoryService repositoryService,
        PullRequestService pullRequestService
    ) {
        this.repositoryService = repositoryService;
        this.pullRequestService = pullRequestService;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PullRequest pullRequest = getPullRequestForPath(req.getPathInfo());

        if (pullRequest == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        resp.setContentType("text/html;charset=UTF-8");

        try {
            soyTemplateRenderer.render(
                resp.getWriter(),
                COMMENTS_TAB_RESOURCE_KEY,
                COMMENTS_TAB_TEMPLATE_NAME,
                ImmutableMap.of("pullRequest", pullRequest)
            );
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private @Nullable PullRequest getPullRequestForPath(String pathInfo) {
        String[] components = pathInfo.split("/");

        if (components.length < 7) {
            return null;
        }

        long pullRequestId;
        try {
            pullRequestId = Long.parseLong(components[6]);
        } catch (NumberFormatException e) {
            return null;
        }

        Repository repository = repositoryService.getBySlug(components[2], components[4]);
        if (repository == null) {
            return null;
        }

        PullRequest pullRequest = pullRequestService.getById(repository.getId(), pullRequestId);

        return pullRequest;
    }
}
