package com.atlassian.bitbucket.server;

public class Constants {
    static final String COMMENTS_TAB_RESOURCE_KEY =
        "com.atlassian.bitbucket.server.pull-request-comments:my-comments-plugin-resources";
    static final String COMMENTS_TAB_TEMPLATE_NAME = "my.comments.plugin.templates.commentsTabTemplate";
}
